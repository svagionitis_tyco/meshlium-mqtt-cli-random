Meshlium MQTT client with random data
=====================================


Export to runnable .jar with dependencies
------------------------------------------

Using Eclipe do the following steps:

1.  Right click on the project, in our case is meshlium-mqtt-cli-random. Choose `Export...`

2.  In the `Choose export destination`, select Java and Runnable JAR file.

3.  In the `Runnable JAR File Specification`,


        a.  In the `Launch configuration:` you can find classes containing a `main(String[])` method.
            In our case will be two classes, the `IBM_MQTT` and `JarRsrcLoader`. We need the `IBM_MQTT`.
            If there is nothing in the dropdown menu, try to `Run as` Java Application and select the
            `IBM_MQTT`.

        b.  In the `Export destination:` select the full path for the exported jar file. The name of the
            file should be `MQTT.jar`.

        c.  In the `Library handling:`, select `Package required libraries in generated JAR` in order
            to include the dependent library, org.eclipse.paho.client.mqttv3-1.0.2.jar, in the jar file.

        d.  Press `Finish`.
