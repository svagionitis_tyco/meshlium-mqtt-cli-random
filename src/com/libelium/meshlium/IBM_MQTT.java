package com.libelium.meshlium;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class IBM_MQTT implements MqttCallback
{
    static int port = 1883;
    static String ip = "localhost";

    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd'T'HH:mm:ss";

    static int sec = 0;
    static int mqtt_error;
    private MqttClient client;
    private String brokerUrl;
    private static boolean quietMode;
    private static int interval;
  
    public static void main(String[] args)
    {
        System.out.println("=============================================================================\n");
        System.out.println("** Libelium IBM MQTT Integration v1.2 **\n");
        System.out.println("=============================================================================\n");

        // Set the interval for sleeping
        interval = 0;

        // Parsing the arguments given in the command line
        parse(args);

        
        // Test create sqlite DB
        // Example from https://github.com/xerial/sqlite-jdbc
        Connection sqliteConnection = null;
        try
        {
            // create a database connection
            sqliteConnection = DriverManager.getConnection("jdbc:sqlite:sample.db");
            Statement statement = sqliteConnection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            
            statement.executeUpdate("drop table if exists person");
            statement.executeUpdate("create table person (id integer, name string)");
            statement.executeUpdate("insert into person values(1, 'leo')");
            statement.executeUpdate("insert into person values(2, 'yui')");
            ResultSet rs = statement.executeQuery("select * from person");
            while(rs.next())
            {
              // read the result set
              System.out.println("name = " + rs.getString("name"));
              System.out.println("id = " + rs.getInt("id"));
            }
        }
        catch(SQLException e)
        {
          // if the error message is "out of memory", 
          // it probably means no database file is found
          System.err.println(e.getMessage());
        }
        finally
        {
          try
          {
            if(sqliteConnection != null)
                sqliteConnection.close();
          }
          catch(SQLException e)
          {
            // connection close failed.
            System.err.println(e);
          }
        }
        
        
        // Create separate clients for sensor, wifi and bluetooth
        // in different threads.
        // TODO Must be a better way to do this
        
        // Create a client to send sensor data        
        Thread sensorThread = new Thread(new Runnable() {
            public void run() {
                sendSensorData();
            }
        });
        sensorThread.start();

        // Create a client to send wifi data
        Thread wifiThread = new Thread(new Runnable() {
            public void run() {
                sendWifiData();
            }
        });
        wifiThread.start();

        // Create a client to send bluetooth data
        Thread bluetoothThread = new Thread(new Runnable() {
            public void run() {
                sendBluetoothData();
            }
        });
        bluetoothThread.start();

    }
  
    /**
     * Connect to the sensor DB and publish the messages to the broker.
     * An MQTT client is created with id LibeliumSensors which publishes
     * the data for the sensors.
     */
    private static void sendSensorData()
    {
        Sensors s = new Sensors(true);
        
        for (;;)
        {
            long start = System.currentTimeMillis();

            // Read messages from sensors
            String msg = s.createCAP(UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                                     UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                                     System.currentTimeMillis(), UUID.randomUUID().toString(),
                                     UUID.randomUUID().toString());

            String topic = s.createTopic(UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                                         UUID.randomUUID().toString());
            
            sendMQTT("LibeliumSensors", topic, msg);

            try {
                long end = System.currentTimeMillis();
                long elapsedTime = end - start;

                long sleepTime = interval * 1000 - elapsedTime;
                if (sleepTime > 0L) {
                    System.out.print("=============sleep " + sleepTime / 1000L + " sec. ==============\n");
                    Thread.sleep(sleepTime);
                } else {
                    System.out.print("=============no sleep, continue==============\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
    }
    
    /**
     * Connect to the wifi DB and publish the messages to the broker.
     * An MQTT client is created with id LibeliumWifi which publishes
     * the data for the wifi.
     */
    private static void sendWifiData()
    {
        Wifi w = new Wifi();
        
        for (;;)
        {
            long start = System.currentTimeMillis();


            // Read messages from wifi
            String msg = w.createCAP(UUID.randomUUID().toString(), System.currentTimeMillis(),
                                     UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                                     UUID.randomUUID().toString(), UUID.randomUUID().toString());

            String topic = w.createTopic();

            sendMQTT("LibeliumWifi", topic, msg);

            try {
                long end = System.currentTimeMillis();
                long elapsedTime = end - start;

                long sleepTime = interval * 1000 - elapsedTime;
                if (sleepTime > 0L) {
                    System.out.print("=============sleep " + sleepTime / 1000L + " sec. ==============\n");
                    Thread.sleep(sleepTime);
                } else {
                    System.out.print("=============no sleep, continue==============\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
    }
    
    /**
     * Connect to the bluetooth DB and publish the messages to the broker.
     * An MQTT client is created with id LibeliumBluetooth which publishes
     * the data for the bluetooth.
     */
    private static void sendBluetoothData()
    {
        Bluetooth b = new Bluetooth();
        
        for (;;)
        {
            long start = System.currentTimeMillis();
            
            // Read messages from bluetooth
            String msg = b.createCAP(UUID.randomUUID().toString(), System.currentTimeMillis(),
                                     UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                                     UUID.randomUUID().toString(), UUID.randomUUID().toString(),
                                     UUID.randomUUID().toString());

            String topic = b.createTopic();

            sendMQTT("LibeliumBluetooth", topic, msg);

            try {
                long end = System.currentTimeMillis();
                long elapsedTime = end - start;

                long sleepTime = interval * 1000 - elapsedTime;
                if (sleepTime > 0L) {
                    System.out.print("=============sleep " + sleepTime / 1000L + " sec. ==============\n");
                    Thread.sleep(sleepTime);
                } else {
                    System.out.print("=============no sleep, continue==============\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
    }
    
    /**
     * Publish a message to the MQTT broker.
     *
     * Set the mqtt_error to 0 if everything is ok,
     * else set it to 1.
     * 
     * @param clientId The client ID. For multithreaded we need different id for each thread.
     * @param topic The topic of the message
     * @param message The message to be published
     */
    private static void sendMQTT(String clientId, String topic, String message)
    {
        String url = "tcp://" + ip + ":" + port;

        try
        {
            mqtt_error = 0;

            IBM_MQTT sampleClient = new IBM_MQTT(url, clientId, quietMode);
            sampleClient.publish(topic, 2, message.getBytes());
        } catch (MqttException me) {
            System.out.println("reason " + me.getReasonCode());
            System.out.println("msg " + me.getMessage());
            System.out.println("loc " + me.getLocalizedMessage());
            System.out.println("cause " + me.getCause());
            System.out.println("excep " + me);
            me.printStackTrace();
            mqtt_error = 1;
        }
    }

    /**
     * Parse the arguments of the command line
     * 
     * Currently there are 2 arguments that can be parsed.
     * *	-i:	The IP of the MQTT broker to connect. If this is not specified
     * 		is defaulting to localhost.
     * *	-p:	The port of the MQTT broker. If not specified is defaulting to 1883.
     * 
     * @param args Arguments of command line
     */
    private static void parse(String[] args)
    {
        for (int i = 0; i < args.length; i++) {
            char arg = 0;
            if ((args[i].length() == 2) && (args[i].startsWith("-"))) {
                arg = args[i].charAt(1);

                if ((i == args.length - 1) || (args[(i + 1)].charAt(0) == '-')) {
                    System.out.println("Missing value for argument: " + args[i]);
                    printHelp();
                    return;
                }
            }

            switch (arg) {
            case 'i':  ip = args[(++i)]; break;
            case 'p':  port = Integer.parseInt(args[(++i)]); break;
            default: 
                System.out.println("Unrecognised argument: " + args[i]);
                printHelp();

                return;
            }

        }
    }
  
    /**
     * Constructor of IBM_MQTT. Create the MQTT client instance.
     * 
     * @param brokerUrl The URL of the MQTT broker.
     * @param clientId A unique name for the MQTT client.
     * @param quietMode If true not print output on screen.
     * @throws MqttException
     */
    public IBM_MQTT(String brokerUrl, String clientId, boolean quietMode)
            throws MqttException
    {
        this.brokerUrl = brokerUrl;
        IBM_MQTT.quietMode = quietMode;

        MemoryPersistence memPersistence = new MemoryPersistence();

        try
        {
            this.client = new MqttClient(this.brokerUrl, clientId, memPersistence);

            this.client.setCallback(this);
        } catch (Exception e) {
            e.printStackTrace();
            log("Unable to set up client: " + e.toString());
            System.exit(1);
        }
    }
  
    /**
     * Publish / send a message to an MQTT server
     * 
     * @param topicName the name of the topic to publish to
     * @param qos the quality of service to delivery the message at (0,1,2)
     * @param payload the set of bytes to send to the MQTT server
     * @throws MqttException
     */
    public void publish(String topicName, int qos, byte[] payload) throws MqttException
    {
        // Connect to the MQTT server
        this.client.connect();
        log("Connected to " + this.brokerUrl + " with client ID " + client.getClientId());

        MqttTopic topic = this.client.getTopic(topicName);

        // Create and configure a message
        MqttMessage message = new MqttMessage(payload);
        message.setQos(qos);

        // Send the message to the server, control is not returned until
        // it has been delivered to the server meeting the specified
        // quality of service.
        log("Publishing to topic \"" + topicName + "\" qos " + qos);
        MqttDeliveryToken token = topic.publish(message);

        token.waitForCompletion();

        // Disconnect the client
        this.client.disconnect();
        log("Disconnected");
    }
  
    /**
     * Subscribe to a topic on an MQTT server
     * Once subscribed this method waits for the messages to arrive from the server
     * that match the subscription. It continues listening for messages until the enter key is
     * pressed.
     * 
     * @param topicName to subscribe to (can be wild carded)
     * @param qos the maximum quality of service to receive messages at for this subscription
     * @throws MqttException
     */
    public void subscribe(String topicName, int qos) throws MqttException
    {
        // Connect to the MQTT server
        this.client.connect();
        log("Connected to " + this.brokerUrl + " with client ID " + this.client.getClientId());

        // Subscribe to the requested topic
        // The QoS specified is the maximum level that messages will be sent to the client at.
        // For instance if QoS 1 is specified, any messages originally published at QoS 2 will
        // be downgraded to 1 when delivering to the client but messages published at 1 and 0
        // will be received at the same level they were published at.
        log("Subscribing to topic \"" + topicName + "\" qos " + qos);
        this.client.subscribe(topicName, qos);

        log("Press <Enter> to exit");
        try {
            System.in.read();
        } catch (IOException localIOException) {}

        // Disconnect the client from the server        
        this.client.disconnect();
        log("Disconnected");
    }
  
    /**
     * Utility method to handle logging. If 'quietMode' is set,
     * this method does nothing
     * 
     * @param message the message to log
     */
    private void log(String message)
    {
        if (!quietMode) {
            System.out.println(message);
        }
    }
  
    /**
     * @see MqttCallback#connectionLost(Throwable) 
     */
    public void connectionLost(Throwable cause)
    {
        // Called when the connection to the server has been lost.
        // An application may choose to implement reconnection
        // logic at this point. This sample simply exits.
        log("Connection to " + this.brokerUrl + " lost! " + "Reason: " + cause.toString());        
    }

    /**
     * @see MqttCallback#deliveryComplete(IMqttDeliveryToken)
     */
    public void deliveryComplete(MqttDeliveryToken token)
    {
        // Called when a message has been delivered to the
        // server. The token passed in here is the same one
        // that was passed to or returned from the original call to publish.
        // This allows applications to perform asynchronous
        // delivery without blocking until delivery completes.
        //
        // This sample demonstrates asynchronous deliver and
        // uses the token.waitForCompletion() call in the main thread which
        // blocks until the delivery has completed.
        // Additionally the deliveryComplete method will be called if
        // the callback is set on the client
        //
        // If the connection to the server breaks before delivery has completed
        // delivery of a message will complete after the client has re-connected.
        // The getPendingTokens method will provide tokens for any messages
        // that are still to be delivered.        
    }

    /**
     * @see MqttCallback#messageArrived(String, MqttMessage)
     */
    public void messageArrived(String topic, MqttMessage message) throws MqttException {
        // Called when a message arrives from the server that matches any
        // subscription made by the client
        String time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(System.currentTimeMillis());
        System.out.println("Time:\t" + time +
                           "  Topic:\t" + topic +
                           "  Message:\t" + new String(message.getPayload()) +
                           "  QoS:\t" + message.getQos());
    }
  
    /**
     * 
     */
    static void printHelp()
    {
        System.out.println(
                "Syntax:\n\njava -jar IBM_MQTT.jar [-a publish|subscribe] [-t <topic>] [-m <message text>]\n [-s 0|1|2] -b <hostname|IP address>] [-p <brokerport>]\n\njava -jar IBM_MQTT.jar -h  Print this help text and quit\n");
    }
  
    
    /**
     * @see MqttCallback#deliveryComplete(IMqttDeliveryToken) 
     */
    public void deliveryComplete(IMqttDeliveryToken paramIMqttDeliveryToken)
    {
        // Called when a message has been delivered to the
        // server. The token passed in here is the same one
        // that was passed to or returned from the original call to publish.
        // This allows applications to perform asynchronous
        // delivery without blocking until delivery completes.
        //
        // This sample demonstrates asynchronous deliver and
        // uses the token.waitForCompletion() call in the main thread which
        // blocks until the delivery has completed.
        // Additionally the deliveryComplete method will be called if
        // the callback is set on the client
        //
        // If the connection to the server breaks before delivery has completed
        // delivery of a message will complete after the client has re-connected.
        // The getPendingTokens method will provide tokens for any messages
        // that are still to be delivered.
    }
}