/**
 * 
 */
package com.libelium.meshlium;

import java.text.SimpleDateFormat;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

/**
 * This class contains variables and methods which will
 * be used to retrieve the wifi data from the DB and create
 * the message to send to the broker.
 *  
 * @author Stavros Vagionitis <svagionitis@tycoint.com>
 *
 */
public class Wifi {

    private int sec = 0;
    private boolean isMsgJson = false;
    
    /**
     * Constructor for the Wifi class.
     * Read the credential file for the DB settings and
     * connects to the DB.
     */
    public Wifi(boolean isMsgJson) {
        // TODO Auto-generated constructor stub
        
        this.isMsgJson = isMsgJson;
    }

    public Wifi() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Create the topic which the message will be published.
     * 
     * The topic for the wifi is Libelium/wifi
     * 
     * @return The topic 
     */
    protected String createTopic()
    {
        String ret = new String();
        
        ret = "Libelium/" + "wifi";
        
        return ret;
    }

    /**
     * Create a message in CAP format which will be sent using MQTT.
     * 
     * More information about CAP see https://en.wikipedia.org/wiki/Common_Alerting_Protocol
     * and https://github.com/google/cap-library
     *  
     * @param id_frame Unique id in the DB.
     * @param timestamp Timestamp of the scan performed.
     * @param mac MAC address of the device.
     * @param ap The access point to which the device is connected.
     * @param rssi The signal strength.
     * @param vendor The vendor of the wireless device.
     * @return A message in CAP format
     */
    protected String createCAP(String id_frame, long timestamp, String mac, String ap, String rssi, String vendor)
    {
        String ret = null;
        String xml = null;

        String time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(timestamp);
        String scanTime = time;

        String timezone = new SimpleDateFormat("Z").format(timestamp);
        timezone = timezone.replace("00", ":00");
        time = time + timezone;

        String ident = "Libelium_" + time + "_" + sec;

        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
                "<cap:alert xmlns:cap=\"urn:oasis:names:tc:emergency:cap:1.2\" " +
                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                    "xsi:schemaLocation=\"urn:oasis:names:tc:emergency:cap:1.2 CAP-v1.2-os.xsd \"> " +
                "<cap:identifier>" + ident + "</cap:identifier> " +
                "<cap:sender>Libelium_Wifi</cap:sender> " + 
                "<cap:sent>" + time + "</cap:sent> " + 
                "<cap:scanTime>" + scanTime + "</cap:scanTime> " +
                "<cap:status>Actual</cap:status> " + 
                "<cap:msgType>Alert</cap:msgType> " + 
                "<cap:scope>Public</cap:scope> " + 
                "<cap:code>KPI</cap:code> " + 
                "<cap:info> " + 
                "<cap:category>Other</cap:category> " + 
                "<cap:event>Libelium</cap:event> " + 
                "<cap:urgency>Immediate</cap:urgency> " + 
                "<cap:severity>Unknown</cap:severity> " + 
                "<cap:certainty>Observed</cap:certainty> " + 
                "<cap:onset>" + time + "</cap:onset> " + 
                "<cap:senderName>Libelium</cap:senderName> " + 
                "<cap:headline>Wifi Scanning</cap:headline> " + 
                "<cap:description>Data from Wifi Scanning: </cap:description> " + 
                "<cap:parameter> " + 
                    "<cap:valueName>ID</cap:valueName> " + 
                    "<cap:value>" + id_frame + "</cap:value> " +
                "</cap:parameter> " +
                "<cap:parameter> " +
                    "<cap:valueName>MAC</cap:valueName> " + 
                    "<cap:value>" + mac + "</cap:value> " +
                "</cap:parameter> " +
                "<cap:parameter> " +
                    "<cap:valueName>AP</cap:valueName> " + 
                    "<cap:value>" + ap + "</cap:value> " +
                "</cap:parameter> " +
                "<cap:parameter> " +
                    "<cap:valueName>RSSI</cap:valueName> " + 
                    "<cap:value>" + rssi + "</cap:value> " +
                "</cap:parameter> " +
                "<cap:parameter> " +
                    "<cap:valueName>Vendor</cap:valueName> " + 
                    "<cap:value>" + vendor + "</cap:value> " + 
                "</cap:parameter> " + 
                "</cap:info> " + 
                "</cap:alert>";

        sec += 1;

        // Check if the message will be json
        if (this.isMsgJson) {
            try {
                // Convert XML to JSON
                JSONObject xmlJSONObj = XML.toJSONObject(xml);
                ret = xmlJSONObj.toString();
            } catch  (JSONException je) {
                je.printStackTrace();
            }
        }
        else {
            ret = xml;
        }

        return ret;
    }

}
