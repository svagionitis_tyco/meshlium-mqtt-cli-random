/**
 * 
 */
package com.libelium.meshlium;

import java.text.SimpleDateFormat;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

/**
 * This class contains variables and methods which will
 * be used to retrieve the sensor data from the DB and create
 * the message to send to the broker.
 * 
 * @author Stavros Vagionitis <svagionitis@tycoint.com>
 * 
 */
public class Sensors {

    private int sec = 0;
    private boolean isMsgJson = false;
    
    /**
     * Constructor for the Sensors class.
     * Read the credential file for the DB settings and
     * connects to the DB. 
     */
    public Sensors(boolean isMsgJson) {
        // TODO Auto-generated constructor stub
        
        this.isMsgJson = isMsgJson;
    }

    public Sensors() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Create the topic which the message will be published.
     * 
     * An example for the battery is Libelium/weather_400575291/BAT
     * 
     * @param id_wasp A name given to a Waspmote.
     * @param id_secret The serial id of the Waspmote.
     * @param sensor The name of the sensor.
     * @return The name of the topic
     */
    protected String createTopic(String id_wasp, String id_secret, String sensor)
    {
        String ret = new String();
        
        ret = "Libelium/" + id_wasp + "_" + id_secret + "/" + sensor;
        
        return ret;
    }

    /**
     * Create a message in CAP format which will be sent using MQTT.
     * 
     * More information about CAP see https://en.wikipedia.org/wiki/Common_Alerting_Protocol
     * and https://github.com/google/cap-library
     * 
     * @param timestamp Timestamp of the message.
     * @param id A unique and increasing number for each sensor data. Primary key for the table.
     * @param id_wasp A name given to a Waspmote, e.g. City, Smart Agriculture, etc
     * @param name The sensor's name, e.g. DATE, TIME, ACC, BAT, etc
     * @param msg The value of the sensor.
     * @param frame_number An increasing number given to each message sent from a Waspmote.
     * @param id_secret The serial id of the Waspmote. It's located beneath the device.
     * @return A message in CAP format
     */
    protected String createCAP(String id, String id_wasp, String id_secret, String frame_number, long timestamp, String name, String msg)
    {
        String ret = null;
        String xml = null;

        String time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(timestamp);

        String timezone = new SimpleDateFormat("Z").format(timestamp);
        timezone = timezone.replace("00", ":00");
        time = time + timezone;

        String ident = "Libelium_" + time + "_" + sec;

        xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
                "<cap:alert xmlns:cap=\"urn:oasis:names:tc:emergency:cap:1.2\" " +
                    "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                    "xsi:schemaLocation=\"urn:oasis:names:tc:emergency:cap:1.2 CAP-v1.2-os.xsd \"> " +
                "<cap:identifier>" + ident + "</cap:identifier> " +
                "<cap:sender>Libelium_" + id_wasp + "_" + id_secret + "</cap:sender> " + 
                "<cap:sent>" + time + "</cap:sent> " + 
                "<cap:status>Actual</cap:status> " + 
                "<cap:msgType>Alert</cap:msgType> " + 
                "<cap:scope>Public</cap:scope> " + 
                "<cap:code>KPI</cap:code> " + 
                "<cap:info> " + 
                "<cap:category>Other</cap:category> " + 
                "<cap:event>Libelium</cap:event> " + 
                "<cap:urgency>Immediate</cap:urgency> " + 
                "<cap:severity>Unknown</cap:severity> " + 
                "<cap:certainty>Observed</cap:certainty> " + 
                "<cap:onset>" + time + "</cap:onset> " + 
                "<cap:senderName>Libelium</cap:senderName> " + 
                "<cap:headline>Waspmote sensors</cap:headline> " + 
                "<cap:description>Sensor data from Waspmote devices: " + name + "</cap:description> " + 
                "<cap:parameter> " + 
                    "<cap:valueName>" + name + "</cap:valueName> " + 
                    "<cap:value>" + msg + "</cap:value> " + 
                "</cap:parameter> " + 
                "</cap:info> " + 
                "</cap:alert>";

        sec += 1;

        // Check if the message will be json
        if (this.isMsgJson) {
            try {
                // Convert XML to JSON
                JSONObject xmlJSONObj = XML.toJSONObject(xml);
                ret = xmlJSONObj.toString();
            } catch  (JSONException je) {
                je.printStackTrace();
            }
        }
        else {
            ret = xml;
        }

        return ret;
    }
    
}
